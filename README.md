# dupkey/movie-scraper

Web scraper to get movie info and scores.

## Run service offline

```bash
docker-compose up
```

## Re-build the image
```bash
docker-compose up --build
```

## Notes
You can access the shells for our images with the following (example) command:

```bash
docker exec -it movie-scraper_serverless_1 /bin/ash
```

`/bin/ash` is Ash ([Almquist Shell](https://www.in-ulm.de/~mascheck/various/ash/#busybox)) provided by BusyBox

### Adminer
Access Adminer via: [http://0.0.0.0:33066](http://0.0.0.0:33066)
Login with the following -
Server: mariadb
Username: root
Password: password

### Database
For know, you can import the earliest SQL file in the "migrations" folder into your database.