SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

CREATE DATABASE `movies` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci */;
USE `movies`;

DROP TABLE IF EXISTS `movie`;
CREATE TABLE `movie` (
  `id` binary(16) NOT NULL,
  `title` varchar(75) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `imdbPath` varchar(75) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rtPath` varchar(75) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `releaseDate` date NOT NULL,
  `crawled` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `imdbPath` (`imdbPath`),
  UNIQUE KEY `rtPath` (`rtPath`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `movieRating`;
CREATE TABLE `movieRating` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `movieId` binary(16) NOT NULL,
  `imdbCount` int(10) unsigned NOT NULL,
  `imdbValue` decimal(2,1) unsigned NOT NULL,
  `rtCount` int(7) unsigned NOT NULL,
  `rtValue` int(3) unsigned NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `movieId` (`movieId`),
  CONSTRAINT `movieRating_ibfk_1` FOREIGN KEY (`movieId`) REFERENCES `movie` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `top`;
CREATE TABLE `top` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `json` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `top` (`id`, `json`) VALUES
(1,	'[]');