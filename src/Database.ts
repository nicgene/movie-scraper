import * as mariadb from 'mariadb';

export default class Database {
  private pool;
  private connection;

  constructor() {
    this.pool = mariadb.createPool({
      host: process.env.MARIADB_HOST,
      user: process.env.MARIADB_USER,
      password: process.env.MARIADB_PASSWORD,
      database: process.env.MARIADB_DATABASE,
      port: 3306,
      connectionLimit: 5
    });
  }
  
  public async query(query: string, params: Array<any> = []) {
    try {
      this.connection = await this.pool.getConnection();
      let rows = await this.connection.query(query, params);
      return rows;
    } catch (error) {
      console.error(error);
    }
  }

  public async connectionEnd() {
    await this.connection.end();
  }
}