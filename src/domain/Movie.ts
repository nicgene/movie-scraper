export default class Movie {
  private id: string
  private title: string
  private description: string | null
  private created: Date
  private imdbPath: string
  private rtPath: string | null
  private releaseDate: Date
  private crawled: Date | null

  constructor(
    id: string,
    title: string,
    description: string = null,
    created: Date,
    imdbPath: string,
    rtPath: string = null,
    releaseDate: Date,
    crawled: Date = null
  ) {
    this.id = id;
    this.title = title;
    this.description = description;
    this.created = created;
    this.imdbPath = imdbPath;
    this.rtPath = rtPath;
    this.releaseDate = releaseDate;
    this.crawled = crawled;
  }

  public getId(): string {
    return this.id;
  }
  
  public getTitle(): string {
    return this.title;
  }

  public getDescription(): string | null {
    return this.description;
  }

  public getCreated(): Date {
    return this.created;
  }

  public getImdbPath(): string {
    return this.imdbPath;
  }

  public getRtPath(): string | null {
    return this.rtPath;
  }

  public setRtPath(rtPath: string): Movie {
    this.rtPath = rtPath;
    return this;
  }

  public getReleaseDate(): Date {
    return this.releaseDate;
  }

  public getCrawled(): Date | null {
    return this.crawled;
  }

  public setCrawled(crawled: Date): Movie {
    this.crawled = crawled;
    return this;
  }
}