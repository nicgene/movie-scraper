export default class Rating {
  private movieId: string
  private imdbCount: number
  private imdbValue: number
  private rtCount: number
  private rtValue: number
  private created: Date

  constructor(
    movieId: string,
    imdbCount: number,
    imdbValue: number,
    rtCount: number,
    rtValue: number,
    created: Date
  ) {
    this.movieId = movieId;
    this.imdbCount = imdbCount;
    this.imdbValue = imdbValue;
    this.rtCount = rtCount;
    this.rtValue = rtValue;
    this.created = created;
  }

  public getMovieId(): string {
    return this.movieId;
  }

  public getImdbCount(): number {
    return this.imdbCount;
  }

  public getImdbValue(): number {
    return this.imdbValue;
  }

  public getRtCount(): number {
    return this.rtCount;
  }

  public getRtValue(): number {
    return this.rtValue;
  }

  public getCreated(): Date {
    return this.created;
  }
}