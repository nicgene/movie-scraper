import * as moment from 'moment';
import * as uuid from 'uuid-parse';
import Database from '../Database';
import Movie from './Movie';
import Rating from './Rating';

export default class Repository {
  private database: Database;

  constructor(database: Database) {
    this.database = database;
  }

  private createMovieInstance(movie) {
    if (movie) {
      return new Movie(
        uuid.unparse(movie.id),
        movie.title,
        movie.description,
        new Date(movie.created),
        movie.imdbPath,
        movie.rtPath,
        new Date(movie.releaseDate),
        new Date(movie.crawled)
      );
    }

    return null;
  }

  public async fetchLastCrawled() {
    let sql = `
      SELECT *
      FROM movie
      WHERE created >= CURDATE() - INTERVAL 6 MONTH
        AND (crawled <= NOW() - INTERVAL 24 HOUR OR crawled IS NULL)
      ORDER BY crawled ASC
      LIMIT 1
    `;

    let result = await this.database.query(sql);
    return this.createMovieInstance(result[0]);
  }

  public async fetchTop() {
    let sql = `
      SELECT json
      FROM top
      ORDER BY id DESC
      LIMIT 1
    `;

    let result = await this.database.query(sql);
    return result;
  }

  public async updateMovieCrawled(id: string) {
    let sql = `
      UPDATE movie
      SET crawled = ?
      WHERE id = UNHEX(REPLACE(?, '-', ''))
    `;

    let params = [
      moment().format('YYYY-MM-DD HH:mm:ss'),
      id
    ];

    await this.database.query(sql, params);
  }

  public async saveMovie(movie: Movie) {
    let sql = `
      INSERT INTO movie
      (id, title, description, created, imdbPath, rtPath, releaseDate, crawled) VALUES
      (UNHEX(REPLACE(?, '-', '')), ?, ?, ?, ?, ?, ?, ?)
    `;

    let params = [
      movie.getId(),
      movie.getTitle(),
      movie.getDescription(),
      moment(movie.getCreated()).format('YYYY-MM-DD HH:mm:ss'),
      movie.getImdbPath(),
      movie.getRtPath(),
      moment(movie.getReleaseDate()).format('YYYY-MM-DD'),
      movie.getCrawled()
    ];

    this.database.query(sql, params);
  }

  public async saveMovieRating(rating: Rating) {
    let sql = `
      INSERT INTO movieRating
      (movieId, imdbCount, imdbValue, rtCount, rtValue, created) VALUES
      (UNHEX(REPLACE(?, '-', '')), ?, ?, ?, ?, ?)
    `;

    let params = [
      rating.getMovieId(),
      rating.getImdbCount(),
      rating.getImdbValue(),
      rating.getRtCount(),
      rating.getRtValue(),
      moment(rating.getCreated()).format('YYYY-MM-DD HH:mm:ss')
    ];

    await this.database.query(sql, params);
  }

  public async saveTop(titles: Array<any>) {
    let sql = `
      INSERT INTO top
      (json) VALUES
      (?)      
    `;

    await this.database.query(sql, [titles])
  }
}