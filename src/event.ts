import { difference } from 'lodash';
import * as uuid from 'uuid';
import Database from './Database';
import Movie from './domain/Movie';
import Rating from './domain/Rating';
import Repository from './domain/Repository';
import IMDb from './scraper/IMDb';
import RT from './scraper/RT';

const imdb = new IMDb();
const rt = new RT();

export const playground = async () => {
  return await {
    statusCode: 200,
    body: JSON.stringify({ message: 'dev/null.' })
  }
}

export const updateTopBoxOffice = async () => {
  let database = new Database()
  let repository = new Repository(database);

  let currentImdbTopMovies = await imdb.fetchTopBoxOffice();
  let previousImdbTopMovies = await repository.fetchTop();
  previousImdbTopMovies = JSON.parse(previousImdbTopMovies[0].json);
  let currentRtTopMovies = await rt.fetchTopBoxOffice();

  let newMovies = difference(currentImdbTopMovies.titles, previousImdbTopMovies);
  if (newMovies.length) {
    await repository.saveTop(currentImdbTopMovies.titles);
  }

  await Promise.all(newMovies.map(async (movie) => {
    let imdbData = currentImdbTopMovies.data.find(x => x.title === movie);
    let imdbDetail = await imdb.fetch(imdbData.imdbPath);
    let rtData = currentRtTopMovies.data.find(x => x.title === movie);
    let movieEntity = new Movie(
      uuid.v4(),
      imdbData.title,
      imdbDetail.description,
      new Date(),
      imdbData.imdbPath,
      rtData.rtPath,
      new Date(imdbDetail.releaseDate)
    );
    await repository.saveMovie(movieEntity);
  }));

  await database.connectionEnd();

  console.log(JSON.stringify({
    details: {
      "New movies found": newMovies.length
    },
    previousImdbTopMovies,
    currentImdbTopMovies: currentImdbTopMovies.titles,
    newMovies
  }));

  return await {
    statusCode: 200,
    body: JSON.stringify({
      details: {
        "New movies found": newMovies.length
      },
      previousImdbTopMovies,
      currentImdbTopMovies: currentImdbTopMovies.titles,
      newMovies
    })
  }
}

export const updateRating = async () => {
  let database = new Database()
  let repository = new Repository(database);
  
  let movie = await repository.fetchLastCrawled();
  if (movie !== null) {
    let imdbData = await imdb.fetch(movie.getImdbPath());
    let rtData = await rt.fetch(movie.getRtPath());
    let rating = new Rating(
      movie.getId(),
      imdbData.aggregateRating.ratingCount,
      imdbData.aggregateRating.ratingValue,
      rtData.aggregateRating.reviewCount,
      rtData.aggregateRating.ratingValue,
      new Date()
    );
    await repository.saveMovieRating(rating);
    await repository.updateMovieCrawled(movie.getId());
    await database.connectionEnd();

    console.log(JSON.stringify({ movie, rating }));

    return await {
      statusCode: 200,
      body: JSON.stringify({ movie, rating })
    }
  }

  console.log(JSON.stringify({ message: 'Everything up to date.' }));

  return await {
    statusCode: 200,
    body: JSON.stringify({ message: 'Everything up to date.' })
  }
}