import axios from 'axios';
import * as cheerio from 'cheerio';

export default class IMDb {
  public async fetch(path: string) {
    let url = 'https://www.imdb.com' + path;
    try {
      let response = await axios.get(url);
      let $ = cheerio.load(response.data);
      let jsonld = JSON.parse($('script[type="application/ld+json"]').html());
      let releaseDate = new Date($('.title_wrapper .subtext').children().last().text().trim().split('(USA)')[0].trim()).toISOString();
      return {
        ...jsonld,
        releaseDate
      }
    } catch (error) {
      console.error(error);
    }
  }

  public async fetchTopBoxOffice() {
    let url = 'https://www.imdb.com/chart/boxoffice';
    let titles = [];
    let data = [];
    try {
      let response = await axios.get(url);
      let $ = cheerio.load(response.data);
      $('#pagecontent table.chart tbody tr').each((index, element) => {
        let title = $('.titleColumn a', element).text();
        let imdbPath = $('.titleColumn a', element).attr('href').split('?')[0];
        titles.push(title);
        data.push({ title, imdbPath });
      });
      return { titles, data };
    } catch (error) {
      console.error(error);
    }
  }
}