import axios from 'axios';
import * as cheerio from 'cheerio';

export default class RT {
  public async fetch(path: string) {
    let url = 'https://www.rottentomatoes.com' + path;
    try {
      let response = await axios.get(url);
      let $ = cheerio.load(response.data);
      let jsonld = JSON.parse($('script[type="application/ld+json"]').html());
      let releaseDate = new Date($('ul.content-meta li .meta-value time').attr('datetime')).toISOString();
      return {
        ...jsonld,
        releaseDate
      }
    } catch (error) {
      console.error(error);
    }
  }

  public async fetchTopBoxOffice() {
    let url = 'https://www.rottentomatoes.com/browse/box-office/';
    let titles = [];
    let data = [];
    try {
      let response = await axios.get(url);
      let $ = cheerio.load(response.data);
      $('section table.table tbody tr').each((index, element) => {
        if (index < 10) {
          let title = $('td', element).eq(3).text().trim();
          let rtPath = $('td', element).eq(3).find('a').attr('href');
          titles.push(title);
          data.push({ title, rtPath });
        }
      });
      return { titles, data };
    } catch (error) {
      console.error(error);
    }
  }
}